﻿using Newtonsoft.Json;

namespace Hardcore.Framework.Models
{
    public class GoogleCloudComputeEngineConfiguration
    {
        [JsonProperty("numberOfInstances")]
        public string NumberOfInstances { get; set; }

        [JsonProperty("whatAreThisInstanceFor")]
        public string WhatAreThisInstanceFor { get; set; }

        [JsonProperty("operatingSystem")]
        public string OperatingSystem { get; set; }

        [JsonProperty("provisioningModel")]
        public string ProvisioningModel { get; set; }

        [JsonProperty("series")]
        public string Series { get; set; }

        [JsonProperty("machineType")]
        public string MachineType { get; set; }

        [JsonProperty("addGpus")]
        public string AddGpus { get; set; }

        [JsonProperty("gpuType")]
        public string GpuType { get; set; }

        [JsonProperty("numberOfGpus")]
        public string NumberOfGpus { get; set; }

        [JsonProperty("localSsd")]
        public string LocalSsd { get; set; }

        [JsonProperty("datacenterLocation")]
        public string DatacenterLocation { get; set; }

        [JsonProperty("committedUsage")]
        public string CommittedUsage { get; set; }
    }
}
