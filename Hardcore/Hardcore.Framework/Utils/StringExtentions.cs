﻿using System.Globalization;
using System.Text.RegularExpressions;

namespace Hardcore.Framework.Utils
{
    public static class StringExtentions
    {
        public static double GetDoubleValueFromPrice(this string price)
        {
            string pattern = @"(\d{1,3}(,\d{3})*(\.\d+)?)";
            Match match = Regex.Match(price, pattern);

            var result = 0d;

            if (match.Success)
            {
                string number = match.Value.Replace(",", "");
                result = double.Parse(number, NumberStyles.Number, CultureInfo.InvariantCulture);
            }
            return result;
        }
    }
}
