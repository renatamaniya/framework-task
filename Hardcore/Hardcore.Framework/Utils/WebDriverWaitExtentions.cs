﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace Hardcore.Framework.Utils
{
    public static class WebDriverWaitExtentions
    {
        public static IWebElement UntilDisplayed(this WebDriverWait wait, Func<IWebDriver, IWebElement> condition)
        {
            return wait.Until(d =>
            {
                var element = condition(d);
                return element != null && element.Displayed ? element : throw new NoSuchElementException();
            });
        }
    }
}
