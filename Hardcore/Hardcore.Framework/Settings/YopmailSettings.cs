﻿using Newtonsoft.Json;

namespace Hardcore.Framework.Settings
{
    public class YopmailSettings
    {
        [JsonProperty("urlYopmailStartPage")]
        public string UrlYopmailStartPage { get; set; }

        [JsonProperty("urlYopmailMailboxPage")]
        public string UrlYopmailMailboxPage { get; set; }

        [JsonProperty("urlYopmailEmailGeneratorPage")]
        public string UrlYopmailEmailGeneratorPage { get; set; }
    }
}
