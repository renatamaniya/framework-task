﻿using Newtonsoft.Json;

namespace Hardcore.Framework.Settings
{
    public class GoogleCloudSettings
    {
        [JsonProperty("urlGoogleCloudStartPage")]
        public string UrlGoogleCloudStartPage { get; set; }

        [JsonProperty("urlGoogleCloudSearchResultPage")]
        public string UrlGoogleCloudSearchResultPage { get; set; }

        [JsonProperty("urlGoogleCloudPricingCalculatorPage")]
        public string UrlGoogleCloudPricingCalculatorPage { get; set; }

        [JsonProperty("pricingCalculatorSearchText")]
        public string PricingCalculatorSearchText { get; set; }
    }
}
