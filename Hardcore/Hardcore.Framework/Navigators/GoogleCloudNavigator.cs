﻿using Hardcore.Framework.Models;
using Hardcore.Framework.PageObjects;
using Hardcore.Framework.Settings;
using Hardcore.Framework.Utils;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace Hardcore.Framework.Navigators
{
    public class GoogleCloudNavigator
    {
        private IWebDriver _driver;
        private WebDriverWait _wait;
        private GoogleCloudSettings _settings;

        public GoogleCloudNavigator(IWebDriver driver, WebDriverWait wait, GoogleCloudSettings settings)
        {
            _driver = driver;
            _wait = wait;
            _settings = settings;
        }

        public double CalculatePriceOfComputeEngine(GoogleCloudComputeEngineConfiguration computeEngine, string emailAddress)
        {
            var googleCloudStartPage = new GoogleCloudStartPage(_driver, _wait, _settings.UrlGoogleCloudStartPage);
            googleCloudStartPage.Navigate();
            googleCloudStartPage.Search(_settings.PricingCalculatorSearchText);

            var searchResultCloudPage = new GoogleCloudSearchResultPage(_driver, _wait, _settings.UrlGoogleCloudSearchResultPage);
            searchResultCloudPage.GoToPricingCalculator();

            var googleCloudPriceCalculator = new GoogleCloudPricingCalculator(_driver, _wait, _settings.UrlGoogleCloudPricingCalculatorPage);
            googleCloudPriceCalculator.FillDataToCalculator(computeEngine);

            var price = googleCloudPriceCalculator.GetEstimatePriceFromCalculator();

            googleCloudPriceCalculator.EmailEstimate(emailAddress);
            return price.GetDoubleValueFromPrice();
        }
    }
}
