﻿using Hardcore.Framework.Settings;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium;
using Hardcore.Framework.PageObjects;
using Hardcore.Framework.Utils;

namespace Hardcore.Framework.Navigators
{
    public class YopmailNavigator
    {
        private IWebDriver _driver;
        private WebDriverWait _wait;
        private YopmailSettings _settings;

        public YopmailNavigator(IWebDriver driver, WebDriverWait wait, YopmailSettings settings)
        {
            _driver = driver;
            _wait = wait;
            _settings = settings;
        }

        public string GenerateEmail()
        {
            var startYopmailPage = new YopmailStartPage(_driver, _wait, _settings.UrlYopmailStartPage);
            startYopmailPage.Navigate();
            startYopmailPage.GetGeneretedEmailAdress();
            var generatEmailPage = new YopmailEmailGeneratorPage(_driver, _wait, _settings.UrlYopmailEmailGeneratorPage);
            var emailAddress = generatEmailPage.GetGeneratedEmailAddress();
            return emailAddress;
        }

        public double GetPriceFromEmail(string emailAddress)
        {
            var startYopmailPage = new YopmailStartPage(_driver, _wait, _settings.UrlYopmailStartPage);
            startYopmailPage.Navigate();
            startYopmailPage.GoToMailbox(emailAddress);
            var yopmailMailboxPage = new YopmailMailboxPage(_driver, _wait, _settings.UrlYopmailMailboxPage);
            return yopmailMailboxPage.GetPriceFromEmail().GetDoubleValueFromPrice();
        }
    }
}
