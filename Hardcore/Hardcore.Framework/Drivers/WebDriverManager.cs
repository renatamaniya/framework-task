﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;

namespace Hardcore.Framework.Drivers
{
    public class WebDriverManager
    {
        public static WebDriver GetDriver(string browser)
        {
            switch (browser)
            {
                case "chrome":
                    {
                        return new ChromeDriver();
                    }
                case "firefox":
                    {
                        return new FirefoxDriver();
                    }
                default:
                    {
                        return new ChromeDriver();
                    }
            }
        }
    }
}
