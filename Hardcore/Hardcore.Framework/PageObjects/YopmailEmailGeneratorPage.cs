﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace Hardcore.Framework.PageObjects
{
    public class YopmailEmailGeneratorPage : BasicPageObject
    {
        public YopmailEmailGeneratorPage(IWebDriver driver, WebDriverWait wait, string url) 
            : base(driver, wait, url)
        {
        }

        private IWebElement GeneratedEmailAddrees => Driver.FindElement(By.XPath("//div[@id='egen']"));
        private IReadOnlyCollection<IWebElement> AdsFrame => Driver.FindElements(By.Id("aswift_6"));
        private IWebElement AdsBox => Driver.FindElement(By.Id("ad_position_box"));

        public string GetGeneratedEmailAddress()
        {
            if (AdsFrame.Count > 0)
            {
                Driver.SwitchTo().Frame(AdsFrame.First());
                AdsBox.Click();
                Driver.SwitchTo().DefaultContent();
            }

            return GeneratedEmailAddrees.Text;
        }
    }
}
