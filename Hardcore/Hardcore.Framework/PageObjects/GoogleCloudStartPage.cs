﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace Hardcore.Framework.PageObjects
{
    public class GoogleCloudStartPage : BasicPageObject
    {
        public GoogleCloudStartPage(IWebDriver driver, WebDriverWait wait, string url) :
            base(driver, wait, url)
        {
        }

        private IWebElement SearchField => Driver.FindElement(By.XPath("//input[contains(@class,'mb2a7b')]"));

        public void Search(string searchText)
        {
            SearchField.Click();
            SearchField.Clear();
            SearchField.SendKeys(searchText);
            SearchField.Submit();
        }
    }
}
