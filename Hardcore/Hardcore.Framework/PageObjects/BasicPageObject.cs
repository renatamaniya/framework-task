﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace Hardcore.Framework.PageObjects
{
    public class BasicPageObject
    {
        protected readonly IWebDriver Driver;
        protected readonly WebDriverWait Wait;
        protected readonly string Url;

        public BasicPageObject (IWebDriver driver, WebDriverWait wait, string url)
        {
            Driver = driver;
            Wait = wait;
            Url = url;
        }

        public void Navigate()
        {
            Driver.Navigate().GoToUrl(Url);
            Driver.Manage().Window.Maximize();
        }
    }
}
