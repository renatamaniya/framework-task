﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace Hardcore.Framework.PageObjects
{
    public class YopmailMailboxPage : BasicPageObject
    {
        public YopmailMailboxPage(IWebDriver driver, WebDriverWait wait, string url) 
            : base(driver, wait, url)
        {
        }

        private IWebElement EmailFrame => Driver.FindElement(By.Id("ifmail"));
        private IWebElement BodyEmail => Wait.Until(d => d.FindElement(By.XPath("//body[contains(@class,'bodymail')]")));
        private IWebElement RefreshButton => Driver.FindElement(By.Id("refresh"));
        private IWebElement PricingCost => Wait.Until(d => d.FindElement(By.XPath("//div[@id='mail']//table//h2")));

        public string GetPriceFromEmail()
        {
            if (NewEmailExists())
            {
                Driver.SwitchTo().Frame(EmailFrame);
                return PricingCost.Text;
            }

            return null;
        }

        private bool NewEmailExists()
        {
            var maxAttemptCount = 10;
            var currentAttempt = 1;

            while(currentAttempt <= maxAttemptCount)
            {
                RefreshButton.Click();

                Driver.SwitchTo().Frame(EmailFrame);
                try
                {
                    if (BodyEmail != null)
                    {
                        Driver.SwitchTo().DefaultContent();
                        return true;
                    }
                }
                catch (NoSuchElementException)
                {
                }

                Driver.SwitchTo().DefaultContent();
                currentAttempt++;
            }

            return false;
        }
    }
}
