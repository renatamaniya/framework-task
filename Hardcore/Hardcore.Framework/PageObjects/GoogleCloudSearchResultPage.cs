﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace Hardcore.Framework.PageObjects
{
    public class GoogleCloudSearchResultPage : BasicPageObject
    {
        public GoogleCloudSearchResultPage(IWebDriver driver, WebDriverWait wait, string url) : 
            base(driver, wait, url)
        {
        }

        private IWebElement CalculatorLink => Driver.FindElement(By.XPath("//div[@class='gs-title']/a"));

        public void GoToPricingCalculator()
        {
            CalculatorLink.Click();
        }
    }
}
