﻿using Hardcore.Framework.Models;
using Hardcore.Framework.Utils;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace Hardcore.Framework.PageObjects
{
    public class GoogleCloudPricingCalculator : BasicPageObject
    {
        private const string ActiveSelectMenuXPath = "//div[contains(@class,'md-select-menu-container') and contains(@class,'md-active') and contains(@class,'md-clickable')]";

        public GoogleCloudPricingCalculator(IWebDriver driver, WebDriverWait wait, string url) :
            base(driver, wait, url)
        {
        }

        private IReadOnlyCollection<IWebElement> CookieButtonOK => Driver.FindElements(By.XPath("//button[@class='devsite-snackbar-action']"));
        private IWebElement TopFrame => Driver.FindElement(By.XPath("//devsite-iframe/iframe"));
        private IWebElement Frame => Driver.FindElement(By.Id("myFrame"));

        private IWebElement InputNumberOfInstances => Driver.FindElement(By.XPath("//input[@ng-model='listingCtrl.computeServer.quantity']"));

        private IWebElement WhatAreThisInstanceFor => Driver.FindElement(By.XPath("//input[@ng-model='listingCtrl.computeServer.label']"));

        private IWebElement SelectOS => Driver.FindElement(By.XPath("//md-select[@ng-model='listingCtrl.computeServer.os']"));
        private IWebElement OptionOS(string option) => Wait.UntilDisplayed(d => d.FindElement(By.XPath($"{ActiveSelectMenuXPath}//md-option/div[contains(text(),'{option}')]")));

        private IWebElement SelectProvisioningModel => Driver.FindElement(By.XPath("//md-select[@ng-model='listingCtrl.computeServer.class']"));
        private IWebElement ProvisioningModelOption(string option) => Wait.UntilDisplayed(d => d.FindElement(By.XPath($"{ActiveSelectMenuXPath}//md-option/div[contains(text(),'{option}')]")));

        private IWebElement SelectSeries => Driver.FindElement(By.XPath("//md-select[@ng-model='listingCtrl.computeServer.series']"));
        private IWebElement SeriesOption(string option) => Wait.UntilDisplayed(d => d.FindElement(By.XPath($"{ActiveSelectMenuXPath}//md-option/div[contains(text(),'{option}')]")));

        private IWebElement SelectMachineType => Driver.FindElement(By.XPath("//md-select[@ng-model='listingCtrl.computeServer.instance']"));
        private IWebElement MachineTypeOption(string option) => Wait.UntilDisplayed(d => d.FindElement(By.XPath($"{ActiveSelectMenuXPath}//md-option/div[contains(text(),'{option}')]")));

        private IWebElement CheckboxAddGPUs => Driver.FindElement(By.XPath("//md-checkbox[@ng-model='listingCtrl.computeServer.addGPUs']"));

        private IWebElement SelectGpuType => Driver.FindElement(By.XPath("//md-select[@ng-model='listingCtrl.computeServer.gpuType']"));
        private IWebElement GpuTypeOption(string option) => Wait.UntilDisplayed(d => d.FindElement(By.XPath($"{ActiveSelectMenuXPath}//md-option/div[contains(text(),'{option}')]")));

        private IWebElement SelectNumberOfGpus => Driver.FindElement(By.XPath("//md-select[@ng-model='listingCtrl.computeServer.gpuCount']"));
        private IWebElement NumberOfGpusOption(string option) => Wait.UntilDisplayed(d => d.FindElement(By.XPath($"{ActiveSelectMenuXPath}//md-option/div[contains(text(),'{option}')]")));

        private IWebElement SelectLocalSsd => Driver.FindElement(By.XPath("//md-select[@ng-model='listingCtrl.computeServer.ssd']"));
        private IWebElement LocalSsdOption(string option) => Wait.UntilDisplayed(d => d.FindElement(By.XPath($"{ActiveSelectMenuXPath}//md-option/div[contains(text(),'{option}')]")));

        private IWebElement SelectDatacenterLocation => Driver.FindElement(By.XPath("//md-select[@ng-model='listingCtrl.computeServer.location']"));
        private IWebElement DatacenterLocationOption(string option) => Wait.UntilDisplayed(d => d.FindElement(By.XPath($"{ActiveSelectMenuXPath}//md-option/div[contains(text(),'{option}')]")));

        private IWebElement SelectCommittedUsage => Driver.FindElement(By.XPath("//md-select[@ng-model='listingCtrl.computeServer.cud']"));
        private IWebElement CommittedUsageOption(string option) => Wait.UntilDisplayed(d => d.FindElement(By.XPath($"{ActiveSelectMenuXPath}//md-option/div[contains(text(),'{option}')]")));

        private IWebElement AddToEstimateButton => Driver.FindElement(By.XPath("//button[@class='md-raised md-primary cpc-button md-button md-ink-ripple']"));
        private IWebElement Price => Driver.FindElement(By.XPath("//md-card-content[@id='resultBlock']//h2[@class='md-flex ng-binding ng-scope']"));
        private IWebElement EmailEstimateButton => Driver.FindElement(By.XPath("//button[@id='Email Estimate']"));

        //EmailYourEstimate
        private IWebElement Email => Driver.FindElement(By.XPath("//input[@ng-model='emailQuote.user.email']"));
        private IWebElement SendEmailButton => Driver.FindElement(By.XPath("//md-dialog-actions//button[@class='md-raised md-primary cpc-button md-button md-ink-ripple']"));

        public object ConfigHelper { get; private set; }

        public void FillDataToCalculator(GoogleCloudComputeEngineConfiguration dataForCalcolator)
        {
            var cookieButton = CookieButtonOK.FirstOrDefault();
            if (cookieButton != null)
            {
                cookieButton.Click();
            }

            Driver.SwitchTo().Frame(TopFrame);

            Driver.SwitchTo().Frame(Frame);

            InputNumberOfInstances.Click();
            InputNumberOfInstances.SendKeys(dataForCalcolator.NumberOfInstances);

            WhatAreThisInstanceFor.Click();
            WhatAreThisInstanceFor.SendKeys(dataForCalcolator.WhatAreThisInstanceFor);

            SelectOS.Click();
            OptionOS(dataForCalcolator.OperatingSystem).Click();

            SelectProvisioningModel.Click();
            ProvisioningModelOption(dataForCalcolator.ProvisioningModel).Click();

            SelectSeries.Click();
            SeriesOption(dataForCalcolator.Series).Click();

            SelectMachineType.Click();
            MachineTypeOption(dataForCalcolator.MachineType).Click();

            CheckboxAddGPUs.Click();

            SelectGpuType.Click();
            GpuTypeOption(dataForCalcolator.GpuType).Click();

            SelectNumberOfGpus.Click();
            NumberOfGpusOption(dataForCalcolator.NumberOfGpus).Click();

            SelectLocalSsd.Click();
            LocalSsdOption(dataForCalcolator.LocalSsd).Click();

            SelectDatacenterLocation.Click();
            DatacenterLocationOption(dataForCalcolator.DatacenterLocation).Click();

            SelectCommittedUsage.Click();
            CommittedUsageOption(dataForCalcolator.CommittedUsage).Click();

            AddToEstimateButton.Click();

            Driver.SwitchTo().DefaultContent();
        }

        public string GetEstimatePriceFromCalculator()
        {
            Driver.SwitchTo().Frame(TopFrame);
            Driver.SwitchTo().Frame(Frame);
            var price = Price.Text;
            Driver.SwitchTo().DefaultContent();

            return price;
        }

        public void EmailEstimate(string emailAddress)
        {
            Driver.SwitchTo().Frame(TopFrame);
            Driver.SwitchTo().Frame(Frame);
            EmailEstimateButton.Click();
            Email.Clear();
            Email.SendKeys(emailAddress);
            SendEmailButton.Click();
            Driver.SwitchTo().DefaultContent();
        }
    }
}
