﻿using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium;

namespace Hardcore.Framework.PageObjects
{
    public class YopmailStartPage : BasicPageObject
    {
        public YopmailStartPage(IWebDriver driver, WebDriverWait wait, string url)
            : base(driver, wait, url)
        {
        }

        private IWebElement AcceptCookies => Driver.FindElement(By.XPath("//div[@id='cons-dialog']//button[@id='accept']"));
        private IWebElement EmailGeneratorButton => Driver.FindElement(By.XPath("//a[@href='email-generator']"));
        private IWebElement EmailAddressInput => Driver.FindElement(By.XPath("//input[@id='login']"));
        private IWebElement VerifyMailboxButton => Driver.FindElement(By.XPath("//button[@class='md']"));

        public void GetGeneretedEmailAdress()
        {
            AcceptCookies.Click();

            EmailGeneratorButton.Click();
        }

        public void GoToMailbox(string emailAddress)
        {
            EmailAddressInput.Clear();
            EmailAddressInput.SendKeys(emailAddress);
            VerifyMailboxButton.Click();
        }
    }
}
