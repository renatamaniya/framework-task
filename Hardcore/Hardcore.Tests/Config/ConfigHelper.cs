﻿using Hardcore.Framework.Models;
using Newtonsoft.Json;
using System.Diagnostics;

namespace Hardcore.Framework.Settings
{
    public class ConfigHelper
    {
        public static GoogleCloudSettings GetGoogleCloudSettings()
        {
            string configFileContent = File.ReadAllText("./Config/GoogleCloudSettings.json");
            return JsonConvert.DeserializeObject<GoogleCloudSettings>(configFileContent);
        }

        public static YopmailSettings GetYopmailSettings()
        {
            string configFileContent = File.ReadAllText("./Config/YopmailSettings.json");
            return JsonConvert.DeserializeObject<YopmailSettings>(configFileContent);
        }

        public static GoogleCloudComputeEngineConfiguration GetComputeEngineConfiguration()
        {
            string configFileContent = File.ReadAllText("./TestData/GoogleCloudComputeEngineConfiguration.json");
            return JsonConvert.DeserializeObject<GoogleCloudComputeEngineConfiguration>(configFileContent);
        }
    }
}
