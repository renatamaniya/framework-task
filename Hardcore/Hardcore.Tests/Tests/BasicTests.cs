﻿using Hardcore.Framework.Settings;
using NUnit.Framework;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hardcore.Framework.Models;
using Hardcore.Framework.Drivers;
using NUnit.Framework.Interfaces;
using OpenQA.Selenium.Support.Extensions;

namespace Hardcore.Tests.Tests
{
    public class BasicTests
    {
        protected IWebDriver Driver;
        protected WebDriverWait Wait;
        protected GoogleCloudSettings GoogleCloudSettings;
        protected YopmailSettings YopmailSettings;
        protected GoogleCloudComputeEngineConfiguration ComputeEngineConfiguration; 

        protected BasicTests()
        {
            GoogleCloudSettings = ConfigHelper.GetGoogleCloudSettings();
            YopmailSettings = ConfigHelper.GetYopmailSettings();
            ComputeEngineConfiguration = ConfigHelper.GetComputeEngineConfiguration();
        }

        [SetUp]
        public void SetupTest()
        {
            var browser = TestContext.Parameters["browser"];
            Driver = WebDriverManager.GetDriver(browser);
            Driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
            Wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));
        }

        [TearDown]
        public void Cleanup()
        {
            if (TestContext.CurrentContext.Result.Outcome.Status == TestStatus.Failed)
            {
                var screenShot = Driver.TakeScreenshot();
                string screenshotFileName = $"screenshot_{TestContext.CurrentContext.Test.MethodName}_{DateTime.Now:yyyy-MM-dd_HH_mm_ss}.png";
                screenShot.SaveAsFile(screenshotFileName);
            }
            Driver.Quit();
        }
    }
}
