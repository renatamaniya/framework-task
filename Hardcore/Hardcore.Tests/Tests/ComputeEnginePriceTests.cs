﻿using Hardcore.Framework.Navigators;
using NUnit.Framework;

namespace Hardcore.Tests.Tests
{
    [TestFixture]
    public class ComputeEnginePriceTests : BasicTests
    {
        [Test]
        [Category("PricingCalculatorTest")]
        public void ComputeEngineIsCalculatedAndSendViaEmail_NewEmailWithCorrectPriceReceived()
        {
            //Arrannge 
            var yopmailNavigator = new YopmailNavigator(Driver, Wait, YopmailSettings);

            var googleNavigator = new GoogleCloudNavigator(Driver, Wait, GoogleCloudSettings);

            //Act
            var newEmail = yopmailNavigator.GenerateEmail();
            var priceOfComputeEngine = googleNavigator.CalculatePriceOfComputeEngine(ComputeEngineConfiguration, newEmail);

            var priceOfComputeEngineFromeEmail = yopmailNavigator.GetPriceFromEmail(newEmail);

            //Assert
            Assert.That(priceOfComputeEngine == priceOfComputeEngineFromeEmail, "Price from email is not equal with pricw from Google Cloud Calculator");
        }

        [Test]
        [Category("PricingCalculatorTest")]
        [Category("SmokeTest")]
        public void FillComputeEngineData_CorrectPriceIsCalculated()
        {
            //Arrange

            //Act

            //Assert
            Assert.True(true);
        }
    }
}
